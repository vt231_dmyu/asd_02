#define _CRT_SECURE_NO_WARNINGS
#include <stdio.h>
#include <stdlib.h>
#include <math.h>

#define a 22695477
#define c 1
#define m 4294967296 // 2^(32)
#define n 250
#define K 40000

unsigned int congruentialGenerator(unsigned int x) {
	return (a * x + c) % m; // ������� ������������� ������
}

unsigned int intervals[n] = { 0 }; // ����� ��� ��������� ������� ���������� ����� � ������� ��������
void calculateFrequencies(int* data, int size) {
	for (int i = 0; i < size; i++) {
		intervals[data[i]]++;
	}
}

int main() {
	system("chcp 1251"); // ������� � ������ �� ��������
	system("cls"); // ������� ���� ������

	int data[K];
	unsigned int x;
	printf("������ ��������� �������� x (x >= 0): "); scanf("%u", &x);
	for (int i = 0; i < K; i++) {
		x = congruentialGenerator(x);
		unsigned int rnd = x % n; // �������� � �������� [0, n)
		data[i] = rnd;
	}

	calculateFrequencies(data, K);
	printf("\n1) ������� ���������: \n");
	for (int i = 0; i < n; i++) {
		printf("����� %d: %u\n", i, intervals[i]);
	}

	printf("\n2) ����������� ����������: \n");
	double probability[n];
	for (int i = 0; i < n; i++) {
		probability[i] = intervals[i] / (double)K;
		printf("����� %d: %f\n", i, probability[i]);
	}

	printf("\n3) ����������� ���������: ");
	double avr = 0;
	for (int i = 0; i < n; i++) {
		avr += intervals[i] * probability[i];
	}
	printf("%f\n", avr);

	printf("\n4) ��������: ");
	double disp = 0;
	for (int i = 0; i < n; i++) {
		disp += (intervals[i] - avr) * (intervals[i] - avr) * probability[i];
	}
	printf("%f\n", disp);

	printf("\n5) C������������������ ���������: ");
	double dev = sqrt(disp);
	printf("%f\n", dev);

	return 0;
}